
release_major = 1
release_minor = 10
release_patch = 17

release_so_abi_rev = 1

# These are set by the distribution script
release_vc_rev = None
release_datestamp = 0
release_type = 'unreleased'
