
This branch (1.10) of Botan is only supported for security fixes until
the end of 2017. Please upgrade to 2.x as soon as possible.


Botan is a C++ library for performing a wide variety of cryptographic
operations. It is released under the 2 clause BSD license; see
doc/license.rst for the specifics. You can file bugs on GitHub
(https://github.com/randombit/botan) or by sending a report to the
botan-devel mailing list. More information about the mailing list is
at http://lists.randombit.net/mailman/listinfo/botan-devel/

You can find documentation online at https://botan.randombit.net/ as
well as in the doc directory in the distribution. Several examples can
be found in doc/examples as well.
